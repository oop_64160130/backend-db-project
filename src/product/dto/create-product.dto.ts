import { IsNotEmpty, Length, Matches } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(5, 16)
  name: string;

  @IsNotEmpty()
  price: number;
}
